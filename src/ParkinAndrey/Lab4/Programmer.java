class Programmer extends Engineer {
	public Programmer( int id, String name, int base, StaffProject project ) {
		super(id, name, base, project);
	}
	
	protected String getPerconalInfo() {
		return "position: Programmer, " + super.getPerconalInfo();
	}
}