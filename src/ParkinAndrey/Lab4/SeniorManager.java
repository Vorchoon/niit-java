class SeniorManager extends ProjectManager{
	public SeniorManager( int id, String name, StaffProject project, int base_for_employee ) {
		super(id, name, project, base_for_employee);
	}
	
	protected String getPerconalInfo() {
		return "position: SeniorManager, project: " + project.GetName();
	}
}