import java.util.*;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

class Staff {
	private ArrayList<Employee> employees;
	private Map<String, StaffProject> projects;
	enum XmlType {EMPLOYEES, PROJECTS};
	
	public Staff() {
		this.employees = new ArrayList<Employee>();
		this.projects = new HashMap<String, StaffProject>();
	}
		
	public void AddEmployee(NodeList e_prorerties) {
		int e_id = 0;
		String e_name = "";
		String e_position = "";
		int e_base = 0;
		int e_base_for_employee = 0;
		String e_project = "";
		StaffProject e_project_obj = null;
					
		for(int j = 0; j < e_prorerties.getLength(); j++) {
			Node e_data = e_prorerties.item(j);
			if (e_data.getNodeType() == Node.TEXT_NODE) 
				continue;
			// Если нода не текст, то это один из параметров книги - печатаем
			switch(e_data.getNodeName()) {
				case "id":
					e_id = Integer.parseInt(e_data.getChildNodes().item(0).getTextContent());
					break;
				case "name":
					e_name = e_data.getChildNodes().item(0).getTextContent();
					break;
				case "position":
					e_position = e_data.getChildNodes().item(0).getTextContent();
					break;
				case "base":
					e_base = Integer.parseInt(e_data.getChildNodes().item(0).getTextContent());
					break;
				case "base_for_employee":
					e_base_for_employee = Integer.parseInt(e_data.getChildNodes().item(0).getTextContent());
					break;
				case "project":
					e_project = e_data.getChildNodes().item(0).getTextContent();
					if( this.projects.containsKey(e_project.toUpperCase()) )
						e_project_obj = projects.get(e_project.toUpperCase());
					break;
				default:
					System.out.println("Error: Unknown employee property: " + e_data.getNodeName());
			}
			
		}
		
		// Создаем работника
		switch( e_position.toUpperCase() ) {
			case "DRIVER":
					employees.add(new Driver(e_id, e_name, e_base));
				break;
			case "CLEANER":
					employees.add(new Cleaner(e_id, e_name, e_base));
				break;
			case "PROGRAMMER":
					employees.add(new Programmer(e_id, e_name, e_base, e_project_obj));
				break;
			case "TESTER":
					employees.add(new Tester(e_id, e_name, e_base, e_project_obj));
				break;
			case "PROJECT_MANAGER":
					employees.add(new ProjectManager(e_id, e_name, e_project_obj, e_base_for_employee));
				break;
			case "SENIOR_MANAGER":
					employees.add(new SeniorManager(e_id, e_name, e_project_obj, e_base_for_employee));
				break;
			case "TEAM_LEADER":
					employees.add(new TeamLeader(e_id, e_name, e_base, e_project_obj, e_base_for_employee));
				break;
			default:
				System.out.println("Error: Unknown position: " + e_position);
		}
		if( e_project_obj != null )
			e_project_obj.AddEmployee();
	}
	
	public void AddProject(NodeList p_prorerties) {
		String p_name = "";
		int p_budget = 0;
					
		for(int j = 0; j < p_prorerties.getLength(); j++) {
			Node data = p_prorerties.item(j);
			if (data.getNodeType() == Node.TEXT_NODE) 
				continue;
			// Если нода не текст, то это один из параметров книги - печатаем
			switch(data.getNodeName()) {
				case "name":
					p_name = data.getChildNodes().item(0).getTextContent();
					break;
				case "budget":
					p_budget = Integer.parseInt(data.getChildNodes().item(0).getTextContent());
					break;
				default:
					System.out.println("Error: Unknown project property: " + data.getNodeName());
			}
			
		}
		
		// Создаем проект
		projects.put(p_name.toUpperCase(), new StaffProject(p_name, p_budget) );
	}
		
	public void readDataFromXml(String filename, XmlType type) {
		try {
			// Создается построитель документа
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			// Создается дерево DOM документа из файла
			Document document = documentBuilder.parse(new File(filename));
	 
			// Получаем корневой элемент
			Node root = document.getDocumentElement();

			// Просматриваем все подэлементы корневого - т.е. книги
			NodeList xml_objs = root.getChildNodes();
			for (int i = 0; i < xml_objs.getLength(); i++) {
				Node xml_obj = xml_objs.item(i);
				if (xml_obj.getNodeType() != Node.TEXT_NODE) {
					NodeList xml_properties = xml_obj.getChildNodes();
					if( type == XmlType.EMPLOYEES)
						this.AddEmployee(xml_properties);
					else if( type == XmlType.PROJECTS) 
						this.AddProject(xml_properties);
						
				}
			}
		} catch (ParserConfigurationException ex) {
			ex.printStackTrace(System.out);
		} catch (SAXException ex) {
			ex.printStackTrace(System.out);
		} catch (IOException ex) {
			ex.printStackTrace(System.out);
		}
	}

	public void CalculateSalary() {		
		for(Employee e: employees) {
			e.setWorktime(20*8);
			e.calculateSalary();
		}
	}

	public void PrintEmployees() {		
		for(Employee e: employees) {
			e.Print();
		}
	}

	public void PrintProjects() {
		for(Map.Entry<String, StaffProject> entry: this.projects.entrySet()){
			entry.getValue().Print();
		}
	}
}