

public class StaffDemo{
	public static void main(String[] args) {
		Staff st = new Staff();
		st.readDataFromXml(args[0], Staff.XmlType.PROJECTS);
		st.readDataFromXml(args[1], Staff.XmlType.EMPLOYEES);
		st.CalculateSalary();
		st.PrintProjects();
		System.out.println();
		st.PrintEmployees();
	}
}