import java.awt.*;
import java.util.*;
import javax.swing.*;

public class LabelJPanel extends JPanel{
	private int d_price, d_weight;
	private String d_label;
	private Dimension dim;
	private int x_dist, y_dist, radius;
	private boolean chancel_btn;
	
	public LabelJPanel(String _d_label, int _d_price, int _d_weight, boolean _chancel_btn){
		this.d_price = _d_price;
		this.d_label = _d_label;
		this.d_weight = _d_weight;
		this.chancel_btn = _chancel_btn;
		
		this.setMinimumSize( new Dimension(50, 50) );
		this.setPreferredSize( new Dimension(100, 100) );
		this.setMaximumSize( new Dimension(150, 150) );
	}
	
	public void paintComponent(Graphics g){
		this.dim = this.getSize();
		x_dist = dim.width/2;
		y_dist = dim.height/2;
		radius = Math.min(x_dist,y_dist);
		
		g.setColor(java.awt.Color.WHITE);
		g.fillRoundRect(10, 10, dim.width-20, dim.height-20, 20, 20);
		g.setColor(new java.awt.Color(139, 69, 19));
		g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		g.drawString(d_label, 15, 50);
		if( !chancel_btn ) {
			g.setFont(new Font("TimesRoman", Font.PLAIN, 25));
			g.drawString(""+d_price+" r", 15, 90);
			g.setFont(new Font("TimesRoman", Font.PLAIN, 25));
			g.drawString(""+d_weight+" gr", 15, 130);
			Image myimg = new ImageIcon("label.png").getImage();
			g.drawImage(myimg, dim.width/3 + 20, dim.height/3, radius+20, radius, null); 
		} else {
			Image myimg = new ImageIcon("chancel.png").getImage();
			g.drawImage(myimg, dim.width/4, dim.height/3, radius, radius, null); 
		}

	}
}
