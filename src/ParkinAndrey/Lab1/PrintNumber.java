class ShowNumber{
	public int num_val;
	public String[] num_arr;
	ShowNumber(int _num_val){
		this.num_val = _num_val;
		this.num_arr= new String[5];
		switch(this.num_val) {
			case 0:
				this.num_arr[0] = "  ***  ";
				this.num_arr[1] = " *   * ";
				this.num_arr[2] = " *   * ";
				this.num_arr[3] = " *   * ";
				this.num_arr[4] = "  ***  ";
				break;
			case 1:
				this.num_arr[0] = "    *  ";
				this.num_arr[1] = "  * *  ";
				this.num_arr[2] = "    *  ";
				this.num_arr[3] = "    *  ";
				this.num_arr[4] = "  **** ";
				break;
			case 2:
				this.num_arr[0] = "  ***  ";
				this.num_arr[1] = " *   * ";
				this.num_arr[2] = "    *  ";
				this.num_arr[3] = "  *    ";
				this.num_arr[4] = " ***** ";
				break;
			case 3:
				this.num_arr[0] = "  ***  ";
				this.num_arr[1] = " *   * ";
				this.num_arr[2] = "    *  ";
				this.num_arr[3] = " *   * ";
				this.num_arr[4] = "  ***  ";
				break;
			case 4:
				this.num_arr[0] = "     * ";
				this.num_arr[1] = "   * * ";
				this.num_arr[2] = "  *  * ";
				this.num_arr[3] = " ******";
				this.num_arr[4] = "     * ";
				break;
			case 5:
				this.num_arr[0] = " ****  ";
				this.num_arr[1] = " *     ";
				this.num_arr[2] = " * **  ";
				this.num_arr[3] = "     * ";
				this.num_arr[4] = " ****  ";
				break;
			case 6:
				this.num_arr[0] = "  ***  ";
				this.num_arr[1] = " *     ";
				this.num_arr[2] = " ****  ";
				this.num_arr[3] = " *   * ";
				this.num_arr[4] = "  ***  ";
				break;
			case 7:
				this.num_arr[0] = " **** ";
				this.num_arr[1] = "    * ";
				this.num_arr[2] = "   *  ";
				this.num_arr[3] = "  *   ";
				this.num_arr[4] = "  *   ";
				break;
			case 8:
				this.num_arr[0] = "  ***  ";
				this.num_arr[1] = " *   * ";
				this.num_arr[2] = "  * *  ";
				this.num_arr[3] = " *   * ";
				this.num_arr[4] = "  ***  ";
				break;
			case 9:
				this.num_arr[0] = "  ***  ";
				this.num_arr[1] = " *   * ";
				this.num_arr[2] = "  **** ";
				this.num_arr[3] = "     * ";
				this.num_arr[4] = "  ***  ";
				break;
			default:
				System.out.println("Error: unexpected number!!!");
		}
	}
	public void Print(){
		for(String row: num_arr)
			System.out.println(row);
	}
	public void PrintRow(int row_num){
		System.out.print(num_arr[row_num]);
	}
}

public class PrintNumber{
	public static void main(String[] args){
		String number = args[0];
		// Заполняем массив чисел
		ShowNumber[] arr = new ShowNumber[10];
		for(int i = 0; i < 10; i ++)
			arr[i] = new ShowNumber(i);
		
		// Отрисовываем введенное пользователем число по строкам 
		for(int row = 0; row < 5; row ++){
			for(char ch: number.toCharArray()){
				int ch_num = Character.getNumericValue(ch);
				arr[ch_num].PrintRow(row);
			}
			System.out.println();
		}
	}
}