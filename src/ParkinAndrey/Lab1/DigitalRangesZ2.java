public class DigitalRangesZ2{
	public static int range_num;
	DigitalRangesZ2(){
		range_num = 0;
	}
	public static void PrintRange(int start_d, int stop_d){
		if (range_num > 0)
			System.out.print(",");
		if (start_d == stop_d)
			System.out.print(start_d);
		else if (stop_d - start_d == 1)
			System.out.print(start_d + "," + stop_d);
		else
			System.out.print(start_d + "-" + stop_d);
		range_num++;
	}
	public static void main(String[] args){
		int start_range = -1;
		int prev_num = -1;
		for (String retval : args[0].split(",")){
			int num  = Integer.parseInt(retval);
			if (start_range == -1)
				start_range = num;
			else if (num > prev_num + 1){
				PrintRange(start_range, prev_num);
				start_range = num;
			}
			prev_num = num;
		}
		if (start_range > 0)
			PrintRange(start_range, prev_num);
	}
}