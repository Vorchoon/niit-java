class Sqrt
{
   double delta;
   double arg;

   Sqrt(double _arg, double _precision) {
      this.arg = _arg;
	  this.delta = 1/Math.pow(10, _precision);
	  //System.out.println("arg =  "+ this.arg + " delta =  "+ this.delta  + "(" + _precision + ")");
   }
   double average(double x,double y) {
      return (x+y)/2.0;
   }
   boolean good(double guess,double x) {
      return Math.abs(guess*guess-x)<delta;
   }
   double improve(double guess,double x) {
      return average(guess,x/guess);
   }
   double iter(double guess, double x) {
      if(good(guess,x))
         return guess;
      else
         return iter(improve(guess,x),x);
   }
   public double calc() {
      return iter(1.0,arg);
   }
}

class CalculateSqrt
{
   public static void main(String[] args)
   {
      double val = Double.parseDouble(args[0]);
	  int precision = Integer.parseInt(args[1]);
      Sqrt sqrt=new Sqrt(val, precision);
      double result=sqrt.calc();
      System.out.format("Sqrt of %f is %."+precision+"f", val, result);
   }
}