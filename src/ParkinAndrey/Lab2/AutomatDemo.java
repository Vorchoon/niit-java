import java.util.*;
import java.io.*;


class Drink{
	public String name;
	public int price;
	
	Drink(String _name, int _price){
		this.name = _name;
		this.price = _price;
	}
}

class Automat{
	private int cash;
	private ArrayList<Drink> menu;
	enum AutomatState {OFF, WAIT, ACCEPT, CHECK, COOK};
	private AutomatState state;
	Automat(String file_name){
		this.cash = 0;
		this.state = AutomatState.OFF;
		this.menu = new ArrayList<Drink>();
		File menu_file = new File(file_name);
		if(menu_file.exists() && menu_file.isFile()){
			try{
				FileReader menu_file_reader = new FileReader(menu_file);
				BufferedReader menu_reader = new BufferedReader(menu_file_reader);
				String line = null;
				while((line = menu_reader.readLine()) != null){
					String[] line_parts = line.split(";");
					if(line_parts.length >= 2){
						this.menu.add(new Drink(line_parts[0], Integer.parseInt(line_parts[1])));
					}
				}
				menu_reader.close();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		else
			System.out.println("File " + file_name + " not found!!!");
	}
	public String GetStateName(){
		switch(state){
			case OFF:
				return "off";
			case WAIT:
				return "wait";
			case ACCEPT:
				return "accept";
			case CHECK:
				return "check";
			case COOK:
				return "cook";
		}
		System.out.println("Error: unknown state!!!");
		return "error";
	}
	
	public boolean On(){
		if(state != AutomatState.OFF){
			System.out.println("Error: On: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.WAIT;
		System.out.println("Automat is ON.");
		return true;
	}
	
	public boolean Off(){
		if(state != AutomatState.WAIT){
			System.out.println("Error: Off: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.OFF;
		System.out.println("Automat is Off.");
		return true;
	}
	
	public boolean Coin(int coin){
		if(state != AutomatState.WAIT && state != AutomatState.ACCEPT){
			System.out.println("Error: Coin: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.ACCEPT;
		cash+=coin;
		System.out.println("Automat cash is " + cash);
		return true;
	}
	
	private boolean Check(int drink_ind){
		if(drink_ind >= menu.size()){
			System.out.println("Error. Drink with index " + drink_ind + " not exist!!!");
			return false;
		}
		if(menu.get(drink_ind).price > cash){
			System.out.println("Error. Not enough money for " + menu.get(drink_ind).name + "(price is " + menu.get(drink_ind).price + ", cash is " + cash + ")");
			return false;
		}
		return true;
	}
	
	public boolean Cancel(){
		if(state != AutomatState.CHECK && state != AutomatState.ACCEPT){
			System.out.println("Error: Cancel: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.WAIT;
		cash = 0;
		System.out.println("Action is cancel. Please, get your money back!!!");
		return true;
	}
	
	private boolean Cook(){
		if(state != AutomatState.CHECK){
			System.out.println("Error: Cook: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.COOK;
		
		try {
			for(int i = 0; i < 20; i++){
				System.out.print(".");
				Thread.sleep(200);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("");
		return true;
	}
	
	private boolean Finish(){
		if(state != AutomatState.COOK){
			System.out.println("Error: Finish: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.WAIT;
		cash = 0;
		System.out.println("Take your drink!!! And good luck.");
		return true;
	}
	
	public boolean Choice(int drink_ind){
		if(state != AutomatState.ACCEPT){
			System.out.println("Error: Choice: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.CHECK;
		if(!Check(drink_ind)) {
			Cancel();
			return false;
		}
		int change = cash - menu.get(drink_ind).price;
		if(change > 0)
			System.out.println("Get your change:" + change);
		Cook();
		Finish();
		return true;
	}
	
	public void PrintState(){
		System.out.println("Current state is " + GetStateName());
	}
	
	public void PrintMenu(){
		for(int ind = 0; ind < menu.size(); ind++)
			System.out.println(ind + ". " + menu.get(ind).name + " - " + menu.get(ind).price);
	}
}

public class AutomatDemo{
	public static void main(String[] args){
		
		Automat a = new Automat(args[0]);
		a.On();
		a.PrintMenu();
		a.Coin(10);
		a.Choice(1);
		a.Coin(10);
		a.Coin(10);
		a.Choice(1);
	}
}